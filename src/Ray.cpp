/**
 * @file Ray.cpp
 * @brief Implmentation of Ray class.
 *
 * @date Oct 25, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#include <cstring>
#include <string>
#include "Ray.hpp"

using std::string;
using std::to_string;
using boost::posix_time::not_a_date_time;
using boost::posix_time::to_iso_extended_string;

Ray::Ray() : data(0) {
  reset();
}

Ray::Ray(size_t max_data_nbytes) : data(max_data_nbytes) {
  reset();
}

void Ray::reset() {
  rotation_idx = 0;
  ray_idx = 0;
  memory_bank = 0;
  proc_time = not_a_date_time;
  time_interval = 0;
  calc_time = not_a_date_time;
  azimuth = 0;
  data.set_data(nullptr, 0);
}

void Ray::set_fields(unsigned int rotation_idx, unsigned int ray_idx, unsigned int memory_bank,
                     ptime proc_time, UINT16 time_interval, UINT16 azimuth, UCHAR packing,
                     const unsigned char* const data_ptr, size_t data_nbytes) {
  // Note calc_time is not set since it's always determined later
  this->rotation_idx = rotation_idx;
  this->ray_idx = ray_idx;
  this->memory_bank = memory_bank;
  this->proc_time = proc_time;
  this->time_interval = time_interval;
  this->azimuth = azimuth;
  this->packing = packing;
  data.set_data(data_ptr, data_nbytes);
}

void Ray::pack() {
  sbuf.clear();
  msgpack::pack(sbuf, *this);
}

bool Ray::is_filtered() {
  return (data.size() == 2 && data[0] == 0xff && data[1] == 0xff);
}
