/**
 * @file HPxStatus.cpp
 * @brief Definitions for the HPxStatus message class.
 *
 * @date Jan 2, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 *
 */

#include "HPxStatus.hpp"
using std::string;

void HPxStatus::pack(ServerState current_state, SPxHPx100Source & hpx_src, unsigned int runtime_sec,
                     unsigned int rays_last_rot, string message) {
  state = current_state;
  avg_prf = hpx_src.GetAvgPRFInstantaneous();
  meas_azimuths = hpx_src.GetMeasAzimuths();
  runtime_s = runtime_sec;
  meas_rays = rays_last_rot;
  this->message = message;
  sbuf.clear();
  msgpack::pack(sbuf, *this);
}
