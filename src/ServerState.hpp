/**
 * @file ServerState.hpp
 * @brief Definition of the ServerState enum class
 *
 * @date Dec 23, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */


#ifndef SRC_SERVERSTATE_HPP_
#define SRC_SERVERSTATE_HPP_

#include <set>
#include <string>
#include "util/errors.hpp"

/**
 * Possible states for HPxStatus
 */
enum class ServerState {
  begin = 0,           //!< Beginning state.
  config_and_start,    //!< The server and DAQ card are being configured and the ray handler is starting.
  running,             //!< The ray handler is running.
  running_alarm,       //!< The ray handler is running, but there is an alarm from the DAQ card.
  stopping,            //!< The ray handler is in the process of stopping.
  stopped,             //!< The ray handler has been stopped; the server is waiting to be resumed or shutdown.
  stopped_config_error,//!< The ray handler could not start due to a configuration error; reconfigure and restart.
  stopped_run_error,   //!< The ray handler stopped due to some run-time error.
  end,                 //!< The server is ending its execution.
};

// Stopped and running states.
// Note that transition states config_and_start, stopping, and end are absent.
const std::set<ServerState> stopped_states{
  ServerState::begin,
  ServerState::stopped,
  ServerState::stopped_config_error,
  ServerState::stopped_run_error
};
const std::set<ServerState> running_states{
  ServerState::running,
  ServerState::running_alarm
};
inline bool is_stopped(ServerState state) {
  return stopped_states.find(state) != stopped_states.end();
}
inline bool is_running(ServerState state) {
  return running_states.find(state) != running_states.end();
}

/// Get string of HPxServerState. Not in the std namespace.
inline std::string state_string(ServerState state) {
  switch (state) {
    case ServerState::begin:
      return "begin";
    case ServerState::config_and_start:
      return "config_and_start";
    case ServerState::running:
      return "running";
    case ServerState::running_alarm:
      return "running_alarm";
    case ServerState::stopping:
      return "stopping";
    case ServerState::stopped:
      return "stopped";
    case ServerState::stopped_config_error:
      return "stopped_config_error";
    case ServerState::stopped_run_error:
      return "stopped_run_error";
    case ServerState::end:
      return "end";
    default:
      return "UNKNOWN [" + std::to_string(int(state)) + "]";
  }
}

#endif /* SRC_SERVERSTATE_HPP_ */
