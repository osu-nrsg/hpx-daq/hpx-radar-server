/**
 * @file HPxServer.hpp
 * @brief Definition of the HPxServer class.
 *
 * @date Sep 26, 2018
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef SRC_HPXSERVER_HPP
#define SRC_HPXSERVER_HPP

// C++/C std
#include <ctime>
#include <memory>
#include <queue>
#include <string>
// 3rd-party
#include <SPxLibData/SPxHPx100Source.h>
// Project headers
#include "util/util.hpp"
#include "HPxConfig.hpp"
#include "HPxStatus.hpp"
#include "RayHandler.hpp"
#include "ServerState.hpp"
#include "SPxErrorHandler.hpp"
#include "ZmqProxy.hpp"

/**
 * Main class for the HPx Radar Server application.
 *
 * HPxServer configures & controlls the DAQ card and manages the ray handler & 0MQ proxy.
 */
class HPxServer {
 public:
  /**
   * Construct a new HPxServer object.
   *
   * Duties:
   * - Set up the SPx library
   * - Create the SPxHPx100Source DAQ card object
   * - Start the ZmqProxy
   * - Create the SPx error handler and install the spx error handler function to the SPx library
   *
   * @param publisher_endpoint  ZMQ socket endpoint for the application.
   *                            E.g `tcp:/<ip>:<port>` (TCP socket)
   *                            or `ipc://<posix path>` (UNIX domain socket).
   * @param hpx_conf_path       Path to TOML-formatted config file (see example_config.toml).
   * @param config_toml         TOML-formatted string with additional config data (supersedes config file).
   * @param daemon              Should the server run in daemon mode?
   * @param spx_debug_flags     HPx card debug flags. See HPx-200 user manual entry on
   *                            `SPxHPx100Source::SetDebug(UINT32 debug)`.
   */
  HPxServer(
      const std::string& publisher_endpoint, const std::string& hpx_conf_path="",
      const std::string& config_toml="", bool daemon_mode = false, UINT32 spx_debug_flags = 0);

  /**
   * Destroy the HPxserver object.
   *
   * Duties:
   * - Ensure the ray handler is stopped.
   * - Destroy the ray handler and the spx error handler.
   * - Stop the ZmqProxy and close all sockets.
   */
  ~HPxServer();

  /**
   * Run the server state machine loop.
   *
   * TODO: Document the state machine.
   */
  void run_server();
 private:
  using StateQueue = std::queue<ServerState>;
  ServerState current_state = ServerState::begin;
  PipePair spx_log_pipe;
  /// Object for configuration, control, and monitoring of the HPx DAQ card
  std::unique_ptr<SPxHPx100Source> hpx_src;
  std::string config_path;
  std::string config_toml;
  /// Pipe for capture SPx lib log info
  /// Internal address for publishing messages to the proxy for distribution to clients.
  const std::string proxy_endpoint = "inproc://zmq-proxy";
  /// Address for external publisher socket.
  std::string pub_endpoint;
  bool stats_enabled = false;
  bool board_configured = false;
  bool board_ready = false;
  std::string config_load_err = "";
  std::string board_config_err = "";
  std::string run_err = "";
  std::string alarm_msg = "";
  std::time_t tstart;
  /// ZeroMQ context used for all sockets in the app.
  zmq::context_t context;
  /// For publishing status and config information. Not for rays.
  zmq::socket_t info_pub;
  /// Thread for funnelling 0MQ messages from all threads to one external publisher socket.
  std::unique_ptr<ZmqProxy> zmq_proxy;
  /// Object to parse & store the configuration and generate CONFIG messages
  HPxConfig hpx_config;
  /// Object to store and get status info and pack STATUS messages
  HPxStatus hpx_status;
  /// Persistent ray handler object for handling rays from the SPxHPx100Source.
  std::unique_ptr<RayHandler> ray_handler_ptr;
  /// Persistent spx error handler object for handling SPx library errors.
  std::unique_ptr<SPxErrorHandler> spxerr_handler_ptr;

  /*
   * HPxServer init subfunctions
   */
  /// Register OS signal handlers.
  void register_signal_handlers();

  /*
   * Overall state transition functions
   */
  /// Load, configure, and start the DAQ.
  int configure_and_start();
  /// Check for run errors and/or control signals to determine the next state.
  void get_next_run_states(StateQueue& stateq, ServerState last_state);

  /*
   * State transition component functions
   * ------------------------------------
   */
  /**
   * Load a new HPxConfig.
   *
   * Sets ::config_load_err if the config is invalid.
   * @return 0 success, -1 invalid config.
   */
  int get_config();

  /**
   * Adjust the status and config period based on whether in daemon mode and watchdog sending is enabled.
   *
   */
  void check_watchdog();

  /**
   * Open and configure the board and install the ray handler.
   *
   * Sets ::board_config_err if there's an error configuring the board.
   *
   * @return 0 success, -1 error configuring the board.
   * @throws HPxServerError if the config is not valid.
   */
  int prepare_for_daq();

  /**
   * Configure the SPxHPx100Source (used in configure_daq).
   *
   * @throws HPxSPxError if a board config action fails.
   */
  void open_and_configure_board();

  /**
   * Start up the ray handler consumer thread and enable the DAQ card.
   *
   * Sets ::board_config_err if any exception is raised.
   * @return 0 success, -1 error
   */
  int start();

  /// Stop producing rays.
  void stop();

  /**
   * Check and reset OS signals
   * @return signal value (SIGINT, SIGTERM, SIGHUP)
   */
  int check_signals();

  /**
   * Check for exceptions in subthreads.
   *
   * Sets ::run_err with the text info on any excepting threads.
   *
   * @return int 0 no error, -1 exception in some thread.
   */
  int check_thread_exceptions();

  /**
   * Check for alarms (e.g. missing TRG, ACP, ARP, etc.)
   * @return int flags 1: trigger, 2: ACP, 4: ARP
   */
  int check_alarms();
  /*
   * ------------------------------------
   * END state transition component functions
   */


  // config/status publish functions
  /// Publish a CONFIG message with the config info to the 0MQ pub socket.
  void publish_config();
  /// Publish a STATUS message with the current status to the 0MQ pub socket.
  void publish_status(const ServerState& server_state);
};

#endif  /* SRC_HPXSERVER_HPP */
