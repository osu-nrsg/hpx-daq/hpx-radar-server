/**
 * @file util/daemon.hpp
 * @brief systemd calls
 *
 * @date Nov 15, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef SRC_UTIL_DAEMON_HPP_
#define SRC_UTIL_DAEMON_HPP_

#include <cstdint>
#include <systemd/sd-daemon.h>
#include "logging.hpp"

static bool hpx_daemon_mode = false;
static uint64_t hpx_watchdog_us = 1000;

inline void init_daemon_mode(bool daemon_mode) {
  hpx_daemon_mode = daemon_mode;
  sd_watchdog_enabled(0, &hpx_watchdog_us);
}

/** Send systemd a notification that the service is starting. **/
inline void notify_starting() {
  if (hpx_daemon_mode) {
    sd_notify(0, "STATUS=Configuring HPx card and preparing for data acquisition");
  }
}

/** Send systemd a notification that the service is running. **/
inline void notify_running() {
  if (hpx_daemon_mode) {
    sd_notify(0, "READY=1\nSTATUS=Server is running");
  }
}

/** Send systemd a notification that the service is restarting. **/
inline void notify_reloading() {
  if (hpx_daemon_mode) {
    sd_notify(0, "REALOADING=1");
  }
}

/** Send systemd a notification that the service is stopping. **/
inline void notify_shutdown() {
  if (hpx_daemon_mode) {
    sd_notify(0, "STOPPING=1");
  }
}

/** Send systemd a notification that shutdown is complete. **/
inline void notify_shutdown_done() {
  if (hpx_daemon_mode) {
    sd_notify(0, "STATUS=Shutdown complete.");
  }
}

/** Send systemd a watchdog notification (heartbeat).
 *
 * @param trigger If true, also send a WATCHDOG=trigger notification
 */
inline void notify_watchdog(bool trigger=false) {
  if (hpx_daemon_mode) {
    sd_notify(0, "WATCHDOG=1");
    SPDLOG_TRACE("WATCHDOG NOTIFY");
    if (trigger) {
      sd_notify(0, "WATCHDOG=trigger");
      SPDLOG_TRACE("WATCHDOG TRIGGER");
    }
  }
}

/** Notify systemd of an error in the service. **/
inline void notify_error(const char* errmsg) {
  if (hpx_daemon_mode && hpx_watchdog_us > 0) {
    sd_notifyf(0, "STATUS=ERROR: %s", errmsg);
  }
}

#endif /* SRC_UTIL_DAEMON_HPP_ */
