/**
 * @file errors.hpp
 * @brief Various error classes used throughout the application.
 *
 * All application errors inherit from HPxError, which inherits from std::runtime_error.
 * HPxSPxError decodes errors from the HPx card API. It inherits from std::exception.
 *
 * @date Oct 31, 2019
 * @author Randall Pittman <Randall.Pittman@oregonstate.edu>
 * @copyright MIT (see LICENSE.txt)
 */

#ifndef SRC_UTIL_ERRORS_HPP_
#define SRC_UTIL_ERRORS_HPP_

#include <exception>
#include <stdexcept>
#include <string>

#include "SPxLibUtils/SPxError.h"

// Base error class for this application
struct HPxError : public std::runtime_error {
  using std::runtime_error::runtime_error;
};

// --- Subclasses of HPxError ---
struct HPxServerError : public HPxError {
  using HPxError::HPxError;
};

struct RayError : public HPxError {
  using HPxError::HPxError;
};

struct RayHandlerError : public HPxError {
  using HPxError::HPxError;
};

struct RayPoolError : public HPxError {
  using HPxError::HPxError;
};

// Simple error type for reporting config errors.
struct ConfigError : public HPxError {
  using HPxError::HPxError;
};

// Simple error type for reporting RayBuffer errors.
struct RayBufferError : public HPxError {
  using HPxError::HPxError;
};

// Simple error type for reporting ZmqProxy errors.
struct ZmqProxyErr : public HPxError {
  using HPxError::HPxError;
};

// --- End subclasses of HPxError ---


/** Exception class to decode SPx error codes **/
class HPxSPxError : public std::exception {
 public:
  HPxSPxError(SPxErrorCode err_code, const std::string & message) {
    _message = ("SPx Error " + std::to_string(err_code) + ": " +
                std::string(SPxGetErrorStringDetail(err_code)) + " | " + message);
  };
  virtual const char* what() const throw() {
    return _message.c_str();
  }
 private:
  std::string _message;
};

/** Exception class to decode C error codes. **/
class CError : public std::exception {
 public:
  const int errnum;
  CError(const std::string & message) : errnum(errno) {
    _message = message + ": " + strerror(errnum);
  };
  virtual const char* what() const throw() {
    return _message.c_str();
  }
 private:
  std::string _message;
};

#endif /* SRC_UTIL_ERRORS_HPP_ */
