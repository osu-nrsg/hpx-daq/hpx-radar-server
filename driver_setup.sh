#!/bin/bash
# ## Base vars
SCRIPTNAME=$(basename $0)
SPXARCHIVE=$(pwd)
DRIVER_NAMES=("Plx9056" "xdma")

# ## Functions
log () {
  >&2 echo "$SCRIPTNAME: $@"
}
logerr () {
  >&2 echo "$SCRIPTNAME ERROR: $@"
}

# Check if root
if [ $EUID -ne 0 ]; then
    logerr "Must be run as root!"
    exit 126
fi

# Get driver name from first arg
DRIVER_NAME_ERR="Must provide one of the following driver names: ${DRIVER_NAMES[@]}"
for name in ${DRIVER_NAMES[@]}; do
    if [ $name == "$1" ]; then
        DRIVER_NAME=$name
    fi
done
if [ -z "$DRIVER_NAME" ]; then
    logerr  $DRIVER_NAME_ERR
    exit 1
fi

install_plx_driver() {
  # Parse the PLX driver version
  if [ -z ${PLX_DRIVER_VER} ]; then
    log "Getting the driver version"
    PLX_H=$SPXARCHIVE/Drivers/Linux/PLX/Include/Plx.h
    if [ ! -f ${PLX_H} ]; then
      logerr "Plx.h cannot be found. Current directory should be the extracted SPx board support library."
      return 1
    fi
    PLX_DRIVER_VER=$(grep -oP '^#define PLX_SDK_VERSION_STRING\s+\"\K[^"]+' ${PLX_H})
    if [ -z "${PLX_DRIVER_VER}" ]; then
      logerr "Cannot extract PLX_DRIVER_VER from the Plx.h file. Supply it to this script manually."
      return 1
    fi
    log "The driver version is $PLX_DRIVER_VER"
  fi
  PLX_DRIVER_NAME=Plx9056
  export PLX_SDK_DIR=/usr/src/${PLX_DRIVER_NAME}-${PLX_DRIVER_VER}

  log "Creating PLX_SDK_DIR: $PLX_SDK_DIR"
  if ! mkdir -p $PLX_SDK_DIR; then
    logerr "Could not create PLX_SDK_DIR: $PLX_SDK_DIR"
    return 1
  fi

  log "Copying PLX SDK to PLX_SDK_DIR"
  if ! cp -r $SPXARCHIVE/Drivers/Linux/PLX/. $PLX_SDK_DIR; then
    logerr "Could not copy the PLX SDK."
    return 1
  fi

  # Remove clear commands from build and load/unload scripts and makefile
  sed -i /\@clear/d $PLX_SDK_DIR/Linux/Makefiles/Targets.def
  sed -i /^clear$/d $PLX_SDK_DIR/Linux/Driver/builddriver
  sed -i /^clear$/d $PLX_SDK_DIR/Bin/Plx_load
  sed -i /^clear$/d $PLX_SDK_DIR/Bin/Plx_unload

  # Python here-script to add error exit code for Plx_load
  # -- start of heredoc --
  python3 - << END
from pathlib import Path

plx_load_path = Path("$PLX_SDK_DIR/Bin/Plx_load")
find_str = '''    echo "ERROR: Module not loaded or no device found"
    echo
    exit
'''
replace_str = '''    echo "ERROR: Module not loaded or no device found"
    echo
    exit 1
'''

plx_load = plx_load_path.read_text()
plx_load = plx_load.replace(find_str, replace_str, 1)
plx_load_path.write_text(plx_load)
END
  # -- end of Python heredoc --

  log "Creating load/unload/build scripts"
  # -- start of heredoc --
  cat << EOF > $PLX_SDK_DIR/load_driver.sh
#!/bin/bash
if [ -z \$PLX_SDK_DIR ]; then
  export PLX_SDK_DIR=$PLX_SDK_DIR
fi
cd \$PLX_SDK_DIR/Bin
./Plx_load 9056
exit \$?
EOF
  # -- end of load_driver.sh heredoc --
  chmod +x $PLX_SDK_DIR/load_driver.sh

  # -- start of heredoc --
  cat << EOF > $PLX_SDK_DIR/unload_driver.sh
#!/bin/bash
if [ -z \$PLX_SDK_DIR ]; then
  export PLX_SDK_DIR=$PLX_SDK_DIR
fi
cd \$PLX_SDK_DIR/Bin
./Plx_unload 9056
exit \$?
EOF
  # -- end of unload_driver.sh heredoc --
  chmod +x $PLX_SDK_DIR/unload_driver.sh

  # -- start of heredoc --
  cat << EOF > $PLX_SDK_DIR/build_driver.sh
#!/bin/bash
if [ -z \$PLX_SDK_DIR ]; then
  export PLX_SDK_DIR=$PLX_SDK_DIR
fi
\$PLX_SDK_DIR/unload_driver.sh
\$PLX_SDK_DIR/clean_driver.sh
cd \$PLX_SDK_DIR/Linux/Driver
./builddriver 9056
exit \$?
EOF
  # -- end of build_driver.sh heredoc --
  chmod +x $PLX_SDK_DIR/build_driver.sh

  # -- start of heredoc --
  cat << EOF > $PLX_SDK_DIR/clean_driver.sh
#!/bin/bash
if [ -z \$PLX_SDK_DIR ]; then
  export PLX_SDK_DIR=$PLX_SDK_DIR
fi
cd \$PLX_SDK_DIR/Linux/Driver
./builddriver 9056 cleanall
exit \$?
EOF
  # -- end of clean_driver.sh heredoc --
  chmod +x $PLX_SDK_DIR/clean_driver.sh

  log "Building the driver"
  if ! $PLX_SDK_DIR/build_driver.sh; then
    logerr "Problem building the driver"
    return 1
  fi

  log "Creating, enabling, and starting the systemd service"
  # -- start of heredoc --
  cat << EOF > /etc/systemd/system/plx-driver-load.service
[Unit]
Description=Load PLX driver for HPx radar card

[Service]
Type=oneshot
ExecStart=/usr/bin/bash $PLX_SDK_DIR/load_driver.sh
ExecStop=/usr/bin/bash $PLX_SDK_DIR/unload_driver.sh
RemainAfterExit=true

[Install]
WantedBy=multi-user.target
EOF
  # -- end of plx-driver-load.service heredoc --

  # Enable and start the systemd service
  if ! ( systemctl daemon-reload &&
        systemctl enable plx-driver-load.service &&
        systemctl start plx-driver-load.service ); then
    logerr "Problem setting up and/or starting systemd service"
    return 1
  fi
  log "Done"

}

install_xdma_driver() {
  # Parse the xdma driver version
  if [ -z ${XDMA_DRIVER_VER} ]; then
    log "Getting the driver version"
    XDMA_VERSION_H=$SPXARCHIVE/Drivers/Linux/XDMA/xdma/version.h
    if [ ! -f ${XDMA_VERSION_H} ]; then
      logerr "version.h for xdma driver cannot be found. Current directory should be the xtracted SPx board support library."
      return 1
    fi
    XDMA_DRV_MOD_MAJOR=$(grep -oP '^#define DRV_MOD_MAJOR\s+\K\d+' ${XDMA_VERSION_H})
    XDMA_DRV_MOD_MINOR=$(grep -oP '^#define DRV_MOD_MINOR\s+\K\d+' ${XDMA_VERSION_H})
    XDMA_DRV_MOD_PATCHLEVEL=$(grep -oP '^#define DRV_MOD_PATCHLEVEL\s+\K\d+' ${XDMA_VERSION_H})
    XDMA_DRIVER_VER=${XDMA_DRV_MOD_MAJOR}.${XDMA_DRV_MOD_MINOR}.${XDMA_DRV_MOD_PATCHLEVEL}
    if [ -z "$XDMA_DRV_MOD_MAJOR" -o -z "$XDMA_DRV_MOD_MINOR" -o -z "$XDMA_DRV_MOD_PATCHLEVEL" ]; then
      logerr "Cannot extract the driver version from the XDMA version.h file. Supply it to this script manually."
      return 1
    fi
    log "The XDMA driver version is $XDMA_DRIVER_VER"
  fi

  DRIVER_DIR=/usr/src/xdma-$XDMA_DRIVER_VER

  if ! mkdir -p $DRIVER_DIR ; then
    logerr "Could not create $DRIVER_DIR"
    return 1
  fi

  log "Copying xdma driver source to $DRIVER_DIR"
  if ! cp -r $SPXARCHIVE/Drivers/Linux/XDMA/. $DRIVER_DIR ; then
    logerr "Could not copy xdma driver source."
    return 1
  fi

  log "Building driver"
  pushd "$DRIVER_DIR/xdma"
  if ! make ; then
    logerr "There was a problem building the driver."
    return 1
  fi
  log "Installing driver"
  if ! make install ; then
    logerr "There was a problem installing the driver"
    return 1
  fi
  popd
  log "Done"

}

if [ $DRIVER_NAME == "xdma" ]; then
  install_xdma_driver
  exit $?
elif [ $DRIVER_NAME == "Plx9056" ]; then
  install_plx_driver
  exit $?
fi


